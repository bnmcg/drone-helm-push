FROM golang:alpine as build
RUN mkdir /src
COPY . /src
WORKDIR /src
RUN go build -o drone-helm-push

FROM alpine
RUN apk update && \
    apk add ca-certificates curl git && \ 
    curl -O https://get.helm.sh/helm-v3.1.2-linux-amd64.tar.gz && \
    tar -xvzf helm-v3.1.2-linux-amd64.tar.gz && \
    mv linux-amd64/helm /bin/helm && \
    mkdir -p /root/.helm/plugins && \
    mkdir -p /root/.config/helm && \
    helm plugin install https://github.com/chartmuseum/helm-push

COPY --from=build /src/drone-helm-push /drone-helm-push

ENTRYPOINT [ "/drone-helm-push" ]