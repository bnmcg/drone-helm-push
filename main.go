package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

func main() {
	chartPath := os.Getenv("PLUGIN_CHART_PATH")
	repositoryURI := os.Getenv("PLUGIN_REPOSITORY_URI")
	repositoryUsername := os.Getenv("DOCKER_USERNAME")
	repositoryPassword := os.Getenv("DOCKER_PASSWORD")

	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		fmt.Println(pair[0])
		fmt.Println(pair[1])
	}

	addArgs := []string{
		"repo",
		"add",
		"repository",
		repositoryURI,
	}

	if repositoryUsername != "" && repositoryPassword != "" {
		addArgs = append(addArgs, "--username", repositoryUsername, "--password", repositoryPassword)
	}

	fmt.Printf("Running with arguments: %s", strings.Join(addArgs, ", "))

	addCmd := exec.Command("/bin/helm", addArgs...)
	output, err := addCmd.CombinedOutput()
	fmt.Printf("%s\n", output)

	if err != nil {
		fmt.Printf("%v", err)
	}

	cmd := exec.Command("/bin/helm", "push", chartPath, "repository")
	output, err = cmd.CombinedOutput()

	fmt.Printf("%s\n", output)

	if err != nil {
		log.Panic(err)
	}
}
